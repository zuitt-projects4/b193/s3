package com.zuitt.b193;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        System.out.print("Please input a number: ");
        int num = input.nextInt();

        int answer = 1;
        int counter = 1;

       while(num > counter){
           answer = answer*counter;
           counter++;
       }
       answer = answer* num;
       System.out.println("The factorial of " + num + " is " + answer);

       //Stretch goals
        int num2 = 0;
        int ans = 1;

        try{

            System.out.print("Please input a number: ");
            num2 = input.nextInt();

            for(int i = 1; i < num2; i++){

                ans = ans * i;

            }

            ans = ans * num2;

        }catch(InputMismatchException e){
            System.out.println("That's not a number.");
        } finally{

            if (num2 < 0){
                System.out.println("The Input Number is a Negative Number");
            } else if (num2 == 0) {
                System.out.println("The Input Number is Zero");
            } else{
                System.out.println("The factorial of " +num2+ " is " + ans);
            }

        }


    }

}
